@SET ASCII27=

@echo off

set origdir=%CD%
cd %USERPROFILE%\.dotfiles

echo.
echo nemovc dotfiles and environment setup
echo.
echo Checking git is installed
echo.
where git >nul 2>nul
IF %ERRORLEVEL% neq 0 (
  echo GIT NOT FOUND
  echo.
  echo Installing Git-2.16.2-64-bit
  echo.
  echo Click through the installer yourself ^(couldn't get it automated properly^)
  call gitinst\Git-2.16.2-64-bit.exe /loadinf=git.inf /silent
  echo.
  echo. Done
  echo.
  echo Restarting install script
  timeout /t 5 >nul
  exit /B
) else (
  echo Git found
  )
echo.
echo Creating .gitconfig link
if exist ..\.gitconfig (del ..\.gitconfig)
copy %userprofile%\.dotfiles\.gitconfig %userprofile%\.gitconfig
rem ensure that the excludes file location is updated correctly
rem because we can't have %variables% in the actual file
git config --global core.excludesfile %userprofile%\.dotfiles\.gitignoreglobal
echo Done
echo.
echo Getting latest data from git
echo git pull
git pull
echo.
echo --------------------------------------------


echo.
echo Installing Atom
WHERE atom >nul 2>nul
IF %ERRORLEVEL% NEQ 0 (
  echo Atom not found
  echo Downloading atom latest
  start /wait mcurl https://atom.io/download/windows_x64 atom.exe
  echo Done
  echo.
  echo Installing atom
  call "download/atom.exe" --silent
  echo Done
) else (
  echo Atom already installed
)
echo.
echo Setting ATOM_HOME env
setx ATOM_HOME %USERPROFILE%\.dotfiles\.atom
echo Done
echo.
call .apm.bat
echo Atom setup done
echo.
echo --------------------------------------------

echo Installing vdesk
WHERE vdesk >nul  2>nul
IF %ERRORLEVEL% neq 0 (
  echo vdesk not found
  echo.
  echo Downloading vdesk
  start /wait mcurl https://github.com/eksime/VDesk/releases/download/v1.2.0/VDeskSetup.msi vdesksetup.msi
  echo Done
  echo.
  echo Installing vdesk - click yes
  start /wait msiexec /i "download\vdesksetup.msi" /passive
  echo Done
) else (
  echo vdesk already installed
)
echo.
echo --------------------------------------------

echo.


echo.
echo Installing node
echo.
where node >nul 2>nul
if %errorlevel% neq 0 (
  echo Node not found
  echo Downloading node 8.11.1
  start /wait mcurl https://nodejs.org/dist/v8.11.1/node-v8.11.1-x64.msi node.msi
  echo Done
  echo Installing node
  echo.
  start /wait msiexec /i "download\node.msi" /qn
  echo Done
  echo
  echo You will need to restart this command window to proceed.
  exit /B
) else (
  echo Node already present
)
echo.
echo Installing key packages
echo.
echo Thank you node, this may require some time.
echo.
call npm install -g vue-cli quasar-cli eslint eslint-config-airbnb babel-eslint jest eslint-config-base nodemon pm2 firebase
echo Done
echo.
echo Running npm update
echo.
call npm update -g
echo.
echo Node setup done
echo.
echo --------------------------------------------


echo.
echo Installing notepad++
echo.
if exist "C:\Program Files\Notepad++\notepad++.exe" (
  echo Already installed.
) else (
  echo Downloading np++ 7.5.6
  start /wait mcurl https://notepad-plus-plus.org/repository/7.x/7.5.6/npp.7.5.6.Installer.x64.exe notepad++.exe
  echo Done
  echo.
  echo Installing
  call download\notepad++.exe /S
  echo Done

)
echo.
echo Creating config hardlink
if exist %APPDATA%\Notepad^+^+\config.xml (
  rmdir /S /Q "%APPDATA%\Notepad++"
)
mklink /j "%APPDATA%\Notepad++" "Notepad++"
echo.
echo Done
echo.
echo --------------------------------------------



echo.
echo Installing Slack
echo.
echo %LOCALAPPDATA%\slack\slack.exe
if not exist "%LOCALAPPDATA%\slack\slack.exe" (
  echo Downloading slack latest
  start /wait mcurl https://downloads.slack-edge.com/releases_x64/SlackSetup.exe slack.exe
  echo Done
  echo Installing
  call download\slack.exe /s
  echo Done
) else (
  echo Slack already installed
)
echo.
echo --------------------------------------------

echo.
echo Adding doskey shortcuts
reg add "HKCU\Software\Microsoft\Command Processor" /v Autorun /D %USERPROFILE%\.dotfiles\env.cmd /f
echo Done
echo.
echo Adding env variables
call env.onetime.cmd
echo Done

echo.
echo Adding firacode
installFira.vbs %DOTFILES%
echo Done
echo.
cd %ORIgdir%
echo Now setup and ready to go.

:: IF %ERRORLEVEL% NEQ 0 ECHO mycommand wasn't found

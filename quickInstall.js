// a quick install that only sets the terminal and git config.
//
// this is designed to be run on SSH hosts etc
//

const fs = require('fs');
const {
  mkln,
  aptInst,
  execWrap,
  fileContentsIdentical,
  log,
} = require('./manage.js');


async function quickInstall(self) {
  if (self) {
    console.log('Quick Install');
  }
  if (process.platform === 'linux') {
    console.log('\nHard linking files from ~ to ~/.dotfiles');
    await mkln(['.gitconfig', '.inputrc', '.bashrc', '.tmux.conf', '.eslintrc.js', '.gitignoreglobal', '.vimrc']);

    console.log('\nInstalling key packages');
    await aptInst(['tmux', 'vim']);

    if (!fs.existsSync('.gitinclude')) {
      console.log('\nCopying .gitinclude');
      execWrap('cp ~/.dotfiles/.gitinclude-linux ~/.dotfiles/.gitinclude');
    } else {
      if (!fileContentsIdentical(['.gitinclude', '.gitinclude-linux'])) {
        log('.gitinclude already exists, and its contents differ from .gitinclude-linux', 'red');
        log('Not overwriting it - fix this yourself :D');
      }
    }

    console.log('\nChecking tmux-plugins');
    if (!fs.existsSync('../.tmux/plugins/tpm')) {
      console.log('Cloning in tmux-plugins');
      await execWrap('git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm');
      await execWrap('~/.tmux/plugins/tpm/script/install_plugins.sh')
    } else {
      log('\ntmux-plugins already present', 'green');
    }
    console.log('\nRebuilding macros');
    await execWrap('node ./expandMacros.js', { stdoutColor: 'green' });
  }
  if (self) {
    log('\nQuick install complete', 'green');
  } else {
    console.log('\nQuick install section complete');
  }
  // fix some permissions issues
  log('\nFixing permissions')
  await execWrap('chown -R $(logname):$(id -g -n $(logname)) .');
  await execWrap('chown -R $(logname):$(id -g -n $(logname)) ~/.config');
  await execWrap('chown -R $(logname):$(id -g -n $(logname)) ~/.tmux');
}

// run if we're not including it
if (require.main === module) {
  quickInstall(true);
}

module.exports = quickInstall;

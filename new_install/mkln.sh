#!/bin/bash
# make a link between <something> and <something>
# make a link between two files (pass source, dest)
# Do nothing silently if the link already exists
# Backup ~/<something> if it is there already

if [ $# -ne 2 ]; then
  echo -e "\e[32m\e[1mmkln missing params\e[0m"
  exit 1
fi

if [ -e $2 ]; then
  if [ -L $2 ] && [ $1 -ef $(readlink $2) ]; then
    echo -e "\e[2m$1\e[0m already present"
    exit
  fi
  # back up the currently existing one
  mv $2 $2.bk-$(date +%F--%T)
  echo -e "Backing up \e[2m$2\e[0m"
fi

echo -e "Linking \e[2m$2\e[0m"
ln -s $1 $2

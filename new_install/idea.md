so, we have

manage: no longer required

quickInstall: 
	sets up linked dotfiles from ~ to ~/.dotfiles
	installs tmux, vim
	links gitinclude
	installs tmux plugins
	fixes permissions

fullInstall
	runs quickinstall
	adds apt repos from .aptrepos
	runs apt update
	adds apt packages from .aptpackages
	installs npm global packages (not sure how we're going to do this, if we use nvm) (actually it should be just fine)
	installs pip packages
	installs firacode font & rebuilds font cache
	creates ~/code and ~/code/self
	installs slack-term, vscode, opera, spotify, discord, slack, pyenv, virtualenv, poetry, 
	links cinnamon spices & spice configs (probably don't want to do this)
	adds autostart scripts
	fixes permissions


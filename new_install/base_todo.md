# Completed setup

Congratulations, you have installed most of the software you need, and configured much of it.

However, there are still a few steps to go through before you're fully set up

## Things which should be automated but aren't yet

- [ ] ` + I to install tmux plugins

## Sign in to services

Obviously, credentials can't be stored in git, so you have to manually connect all of these services you use

- [ ] Google
- [ ] Facebook
- [ ] Outlook
- [ ] Slack
- [ ] GitLab
- [ ] Bitbucket
- [ ] Spotify
- [ ] Bitwarden
  - [ ] In brave-browser
  - [ ] In app
  - [ ] In CLI (bw)
  - Don't forget, when signing into Bitwarden that you need to update the settings to increase security
- [ ] Add id_rsa (from somewhere else)
- [ ] Steam
- [ ] Discord
- [ ] Docker (run `installDockerCredentials` and then `docker login nexus.gwdc.org.au`)

## Set up some things

These are mostly things which are credential-related but aren't just signing into a service

- [ ] spt-config - Just open up spt and follow the instructions
- [ ] [Set some credentials for spotifyd](https://github.com/dspearson/librespot-auth)

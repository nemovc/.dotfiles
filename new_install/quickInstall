#!/bin/bash

# A quick install that only sets the terminal and git config

# designed to be run on SSH hosts, etc
# should be compatible with any system that runs apt

cd ~/.dotfiles/new_install
. colors.sh
# Title and confirm
if [ -z $1 ]; then
  echo -e "\n${GREEN}${BOLD}Quick Install${RESET}"
  echo Enter to continue, or Ctrl + C to exit
  read
else
  echo -e "\n${GREEN}Quick-install section${RESET}"
fi

echo -e "\n${BOLD}Hard linking files from ${FAINT}~${RESET}${BOLD} to ${FAINT}~/.dotfiles${RESET}\n"
echo -e "\n${FAINT}Also setting up directories${RESET}"
./mkln.sh ~/.dotfiles/.gitconfig ~/.gitconfig
./mkln.sh ~/.dotfiles/.inputrc ~/.inputrc
./mkln.sh ~/.dotfiles/.bashrc ~/.bashrc
./mkln.sh ~/.dotfiles/.tmux.conf ~/.tmux.conf
./mkln.sh ~/.dotfiles/.gitignoreglobal ~/.gitignoreglobal
./mkln.sh ~/.dotfiles/.gitinclude-linux ~/.gitinclude
./mkln.sh ~/.dotfiles/.vimrc ~/.vimrc

touch ~/.dotfiles/touches/.motd
mkdir -p ~/.config

echo -e "\n${BOLD}Installing key packages${RESET}"

sudo apt install -y vim tmux curl wget apt-transport-https

echo -e "\n${BOLD}Installing nvm${RESET}"

mkdir -p ~/.nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"                   # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

echo -e "\n${BOLD}Installing node${RESET}"
nvm install node
nvm use node
corepack enable
echo "node v$(node --version)"
echo "npm v$(npm --version)"
echo "npx v$(npx --version)"
echo "yarn v$(yarn --version)"

echo -e "\n${BOLD}Rebuilding aliases${RESET}"
pushd ~/.dotfiles
node expandMacros.js
popd

echo -e "\n${BOLD}Installing neovim & nvim.lazy${RESET}"
sudo mkdir /opt/nvim
if [ ! -d /opt/nvim-linux-x86_64/ ]; then
  curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux-x86_64.tar.gz
  sudo tar -C /opt -xzf nvim-linux-x86_64.tar.gz
fi
sudo ./mkln.sh ~/.dotfiles/nvim.appimage /opt/nvim/nvim
sudo ./mkln.sh ~/.dotfiles/.config/nvim ~/.config/nvim

echo -e "\n${BOLD}Installing LazyGit${RESET}"
pushd ~/.dotfiles/download
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')
curl -Lo lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"
tar xf lazygit.tar.gz lazygit
sudo install lazygit /usr/local/bin
popd

echo -e "\n${GREEN}Quick Install Completed${RESET}, inshallah"

if [ -z $1 ]; then
  echo -e "\n${RED}Restart shell now.${RESET}"
fi

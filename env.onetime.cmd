::setup the aliases
@echo off

setx et c:\eante\vue\tieup
setx ea c:\eante
setx ev c:\eante\vue
setx el c:\eante\lodestar
setx eb c:\eante\supernova-licensing-backend
setx ef c:\eante\supernova-licensing-frontend
setx em c:\eante\supernova-mine-panel
setx jsd %userprofile%\jsdoodles

setx uprof %USERPROFILE%
setx dotfiles %USERPROFILE%\.dotfiles
setx doc %USERPROFILE%\Documents
setx appd %APPDATA%
setx appl %LOCALAPPDATA%
setx desk %USERPROFILE%\Desktop
setx pf "%Programfiles%"
setx pf32 "%programfiles(x86)%"
setx od %ONEDRIVE%
setx tmp %TEMP%
setx down %USERPROFILE%\Downloads

setx opl %appl%\Programs\Opera\launcher.exe

@echo off
echo Installing atom packages.
echo.
FOR /F "tokens=1 delims=@" %%i in (.atompackages) do (
	call :inst "%%i"
)
echo.
echo Running apm upgrade
call apm upgrade
echo.
echo Done

GOTO :eof

:inst 
if exist .atom/packages/%1 (
	echo found %1
) else (
	echo Not found %1
	apm install %1
)
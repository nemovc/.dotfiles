// Set our base paths for things
//
const eantePath = {
  linux: "~/code/eante/",
  windows: "C:\\eante\\",
}[process.platform];

const browserCmd = {
  linux: "brave ",
  windows: "%opl% ",
}[process.platform];

/*
  Inspiration sources include:
  https://github.com/ericandrewlewis/dotfiles-linux-mint/blob/master/.aliases
*/
module.exports = {
  TIME: {
    linux: "node ~/code/timeforcrab",
  },
  "-- -": {
    // note the -- at the start, so it will print as `alias -- -=cd -`
    // and will therefore work on '-'
    linux: "cd -",
  },

  pd: {
    linux: "popd",
  },
  doc: {
    windows: "cd %USERPROFILE%\\Documents",
    linux: "cd ~/Documents",
  },
  desk: {
    windows: "cd %USERPROFILE%\\Desktop",
    linux: "cd ~/Desktop",
  },
  down: {
    windows: "cd %USERPROFILE%\\Downloads",
    linux: "cd ~/Downloads",
  },
  tmp: {
    windows: "cd %TEMP%",
    linux: "cd /tmp",
  },

  // moved out of .bashrc
  ll: {
    linux: "ls -laHF",
  },
  l: {
    linux: "ls -laHF",
  },
  ls: {
    linux: "ls --color=auto",
  },
  grep: {
    linux: "grep --color=auto",
  },
  // alert: {
  //   linux: `notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\\''s /^\\s*[0 - 9]\\+\\s*//;s/[;&|]\\s*alert$//'\\'')"`,
  // },

  appd: {
    comment: "Jump to folder (Windows)",
    windows: "cd %APPDATA%",
  },
  appl: {
    windows: "cd %LOCALAPPDATA%",
  },
  pf: {
    windows: "cd %Programfiles%",
  },
  pf32: {
    windows: "cd %programfiles(x86)%",
  },
  od: {
    windows: "cd %ONEDRIVE%",
  },

  x: {
    windows: "exit",
    linux: "tmux detach || exit",
    comment: "Terminal commands",
    spacingBefore: 1,
  },
  // for the following, refer to .bash_functions
  // for bash functions that these aliases reference
  c: {
    windows: "start cmd.exe /c $*",
    linux: "__nemo_c",
  },
  k: {
    windows: "start cmd.exe /k $*",
    linux: "__nemo_k",
  },
  kc: {
    windows: "start cmd.exe /k cd $*",
    linux: "__nemo_kc",
  },
  kx: {
    windows: "start cmd.exe /k $* & exit",
    linux: "__nemo_kx",
  },
  mkcd: {
    windows: "mkdir $1 && cd $1",
    linux: "__nemo_mkcd",
  },

  au: {
    both: "apm update",
  },
  v: {
    linux: "vim",
  },
  sv: {
    linux: "sudo vim",
  },
  np: {
    windows: "notepad++ $*",
  },
  // to avoid conflict with np package from npm
  "n+": {
    linux: "__nemo_np",
  },
  // to avoid conflict with npx package from npm
  nppx: {
    linux: "__nemo_nppx",
  },
  nq: {
    linux: "__nemo_nq",
  },
  nqx: {
    linux: "__nemo_nqx",
  },

  ni: {
    linux: "npm install",
    windows: "npm install  $*",
  },
  nid: {
    linux: "npm install --save-dev",
    windows: "npm install --save-dev $*",
  },
  nci: {
    linux: "npm ci",
    windows: "npm ci",
  },
  nu: {
    linux: "npm update",
    windows: "npm update  $*",
  },
  nig: {
    linux: "npm install -g",
    windows: "npm install -g  $*",
  },
  nug: {
    linux: "npm update -g",
    windows: "npm update -g  $*",
  },
  nl: {
    linux: "npm list",
    windows: "npm list  $*",
  },
  nlg: {
    linux: "npm list -g",
    windows: "npm list -g  $*",
  },
  nl0: {
    linux: "npm list --depth=0",
    windows: "npm list --depth=0 $*",
  },
  nl0g: {
    linux: "npm list -g --depth=0",
    windows: "npm list -g --depth=0 $*",
  },
  nr: {
    linux: "npm run ",
    windows: "npm run  $*",
  },
  no: {
    linux: "node",
    windows: "node $*",
  },
  nd: {
    linux: "nodemon",
    windows: "nodemon $*",
  },
  ndi: {
    linux: "nodemon --inspect",
    windows: "nodemon --inspect $*",
  },
  nrm: {
    linux: "npm remove",
    windows: "npm remove $*",
  },
  nrmg: {
    linux: "npm remove -g",
    windows: "npm remove $* -g",
  },
  nrt: {
    both: "npm run test",
  },
  nrs: {
    both: "npm run start",
  },
  nrd: {
    both: "npm run dev",
  },

  y: {
    both: "yarn",
  },
  ya: {
    both: "yarn add",
  },
  yad: {
    both: "yarn add --dev",
  },
  yci: {
    both: "yarn add --frozen-lockfile",
  },
  yu: {
    both: "yarn upgrade",
  },
  yl: {
    both: "yarn list",
  },
  yl0: {
    both: "yarn list --depth 0",
  },
  yr: {
    both: "yarn run",
  },
  yrs: {
    both: "yarn start",
  },
  yrt: {
    both: "yarn test",
  },
  yrm: {
    both: "yarn remove",
  },

  fs: {
    linux: "firebase serve",
    windows: "firebase serve  $*",
    comment: "Firebase functions",
    spacingBefore: 1,
  },
  fd: {
    linux: "firebase deploy",
    windows: "firebase deploy  $*",
  },
  fsf: {
    linux: "firebase serve --only functions",
    windows: "firebase serve --only functions  $*",
  },
  fdf: {
    linux: "firebase deploy --only functions",
    windows: "firebase deploy --only functions  $*",
  },
  fdh: {
    linux: "firebase deploy --only hosting",
    windows: "firebase deploy --only hosting  $*",
  },

  qs: {
    linux: "quasar serve dist/spa-mat",
    windows: "quasar serve dist/spa-mat  $*",
    comment: "Quasar comments",
    spacingBefore: 1,
  },
  qd: {
    linux: "quasar dev",
    windows: "quasar dev  $*",
  },
  qde: {
    linux: "quasar dev -m electron",
    windows: "quasar dev -m electron  $*",
  },
  qdl: {
    linux: "quasar dev local-server",
    windows: "quasar dev local-server  $*",
  },
  qdle: {
    linux: "quasar dev -m electron local-server",
    windows: "quasar dev -m electron local-server  $*",
  },
  qb: {
    linux: "quasar build",
    windows: "quasar build  $*",
  },
  qbe: {
    linux: "quasar build -m electron",
    windows: "quasar build -m electron  $*",
  },
  qbd: {
    linux: "quasar build dev-server",
    windows: "quasar build dev-server  $*",
  },
  qbde: {
    linux: "quasar build -m electron dev-server",
    windows: "quasar build -m electron dev-server  $*",
  },
  qbep: {
    linux: "quasar build -m electron && postbuild.sh",
    windows: "quasar build -m electron && postbuild.bat",
  },

  j: {
    linux: "jest",
    windows: "jest  $*",
    comment: "Jest commands",
    spacingBefore: 1,
  },
  jw: {
    linux: "jest --watchAll",
    windows: "jest --watchAll  $*",
  },

  slack: {
    windows: "%appl%\\slack\\slack.exe",
    comment: "Windows launchers for apps",
    spacingBefore: 1,
  },
  spotify: {
    windows: "%appd%\\spotify\\spotify.exe",
    linux: "spotify &",
  },

  // all of the following missing 'linux' versions require a more complex bash function to work
  bb: {
    windows: `${browserCmd} $*`,
    linux: `__nemo_bb`,
    comment: "Web shortcuts\n# opera",
    spacingBefore: 2,
  },
  bbn: {
    windows: `${browserCmd} --new-window $*`,
    linux: `__nemo_bbn`,
  },
  bbx: {
    windows: `${browserCmd} $* $T timeout 0.01 $T exit`,
    linux: "__nemo_bbx",
  },
  bbnx: {
    windows: `${browserCmd} --new-window $* $T timeout 0.01 $T exit`,
    linux: "__nemo_bbnx",
  },

  pr: {
    comment: "Python stuff",
    both: "poetry run ",
  },
  prp: {
    both: "poetry run python",
  },
  djm: {
    comment: "django stuff",
    both: "poetry run python manage.py",
  },
  djshell: {
    both: "poetry run python manage.py shell_plus",
  },
  djrun: {
    both: "poetry run python manage.py runserver",
  },
  djpytest: {
    both: "poetry run pytest",
  },
  "pytest-watch": {
    both: "poetry run pytest-watch",
  },
  vmdh: {
    linux: "source ~/.venv/mdh/bin/activate",
  },

  g: {
    windows: "git $*",
    linux: "git",
    comment: "Git",
    spacingBefore: 1,
  },
  gp: {
    both: "git pull",
  },
  ga: {
    both: "git add",
  },
  "ga.": {
    both: "git add .",
  },
  gc: {
    both: "git commit -v",
  },
  gca: {
    both: "git commit -va",
  },
  gcau: {
    both: "git commit -va && git push",
  },
  gcm: {
    windows: "git commit -m $1",
    linux: "git commit -m",
  },
  gu: {
    both: "git push",
  },
  guu: {
    linux: "git push --set-upstream origin HEAD",
  },
  gd: {
    both: "git diff",
  },
  // shorter command for shorter output
  gs: {
    both: "git status -s",
  },
  gss: {
    both: "git status",
  },
  gl: {
    both: "git log",
  },
  glc: {
    both: "git log --pretty=oneline --abbrev-commit",
  },
  glg: {
    both: "git log --pretty=oneline --abbrev-commit --graph",
  },
  gf: {
    both: "git fetch",
  },
  gk: {
    both: "git checkout",
  },
  gkd: {
    both: "git checkout develop",
  },
  gb: {
    both: "git branch",
  },
  gbk: {
    linux: "__nemo_gbk",
  },
  gbg: {
    linux: "git branch | grep",
  },
  gm: {
    both: "git merge",
  },
  gsf: {
    both: "git submodule foreach",
  },
  gsu: {
    both: "git submodule update",
  },
  gr: {
    both: "git rm",
  },
  grc: {
    both: "git rm --cached",
  },
  grm: {
    // remove all branches already merged to master or develop
    // TODO: test this works
    both: "git branch --merged | egrep -v '(^\\*|master|develop)' | xargs -n 1 git branch -d",
  },
  lg: {
    linux: "lazygit",
  },

  mv: {
    windows: "move $*",
    comment: "'Linux-like' commands for windows",
    spacingBefore: 1,
  },
  rm: {
    windows: "del $*",
  },
  cp: {
    windows: "copy $*",
  },
  cat: {
    windows: "type $*",
  },

  llg: {
    linux: "__nemo_llg",
  },
  ld: {
    linux: "ls -lF | grep --color=never '^d'",
  },
  pg: {
    linux: "ps aux | grep --color=auto -i",
  },
  slowdown: {
    linux: "__nemo_sleepcat",
  },
  r: {
    // eslint-disable-next-line no-useless-escape
    linux: "echo 'Reloaded .bashrc'; source ~/.bashrc",
  },
  ra: {
    linux: "pushd ~/.dotfiles; node ~/.dotfiles/expandMacros.js; popd",
  },
  rt: {
    // eslint-disable-next-line no-useless-escape
    linux: "echo 'Reloaded .tmux.conf';tmux source-file ~/.tmux.conf",
  },

  ipr: {
    windows: "%dotfiles%\\ipr.bat",
    comment: "Windows bat file shortcuts",
    spacingBefore: 1,
  },
  nemoDev: {
    windows: "%dotfiles%\\setupNemoDevEnv.bat",
  },

  grep: {
    linux: "grep --color=auto",
    comment: "Linux-specific command aliases",
    spacingBefore: 1,
  },
  fgrep: {
    linux: "fgrep --color=auto",
  },
  egrep: {
    linux: "egrep --color=auto",
  },
  o: {
    linux: "xdg-open ",
  },
  sudo: {
    linux: "sudo ",
    comment:
      "Make sudo source our .bashrc\n# https://unix.stackexchange.com/questions/148545/why-does-sudo-ignore-aliases",
  },
  please: {
    linux: "sudo \\$(fc -ln -1)",
  },
  hs: {
    linux: "history | grep -i",
  },
  timer: {
    linux:
      'echo \\"Timer started. Stop with Ctrl-D.\\" && date && /usr/bin/time -f "%E" cat && date',
    comment: "Stopwatch",
  },
  afk: {
    linux: "__nemo_afk",
  },
  "online?": {
    linux: "__nemo_online",
  },
  "weather?": {
    linux: "curl -s https://wttr.in/Ballarat",
  },
  "weather2?": {
    linux: "curl -s https://v2.wttr.in/Ballarat",
  },
  postman: {
    linux: "postman &> /dev/null &",
  },

  ta: {
    linux: "__nemo_ta",
    comment: "tmux shortcuts",
    spacingBefore: 1,
  },
  tk: {
    linux: "__nemo_tk",
  },
  tka: {
    linux: "tmux kill-server",
  },
  tko: {
    linux: "tmux kill-session -a",
  },
  t: {
    linux: "tmux",
  },
  tls: {
    linux: "tmux ls",
  },
  lcat: {
    linux: "lolcat",
  },
  mc: {
    linux: "/opt/minecraft-launcher/minecraft-launcher & ",
  },
  mindustry: {
    linux: "pushd ~/code/Mindustry; ./gradlew desktop:run; popd",
  },
  myip: {
    linux:
      'echo \\"local: \\$(hostname -I)\\"; echo \\"remote: \\$(curl -s ifconfig.me)\\"',
  },
  calc: {
    windows: "calc",
    linux: "gnome-calculator &",
  },
  ag: {
    linux: "alias | grep ",
  },
  clip: {
    linux: "xclip -sel clip",
  },
  every: {
    linux: "__nemo_every",
  },
  every: {
    linux: "__nemo_every",
  },
  TIME: {
    linux: "node ~/code/self/timeforcrab/index.js",
  },
  protontricks: {
    linux: "flatpak run com.github.Matoking.protontricks",
  },
  ctjs: {
    linux: "pushd ~/code/ctjs/;gulp;popd",
  },
  civ5: {
    linux:
      "pushd ~/.steam/debian-installation/steamapps/common/Sid\\ Meier\\'s\\ Civilization\\ V;./Civ5XP;popd",
  },
  wow: {
    linux: "wine ~/.wine/drive_c/Program\\ Files\\ \\(x86\\)/World\\ of\\ Warcraft/_retail_/Wow.exe",
  },
  modev: {
    linux: 'mosh -p 30000:31000 owen-dev'
  },
  lodev: {
    linux: 'ssh -E /dev/null owen-dev /bin/sh '
  }
};

# Shell functions for more complex aliases

# Terminal commands
__nemo_c () {
  gnome-terminal -- bash -c "$*"
}

__nemo_k () {
  if [ $# -eq 0 ]; then
    gnome-terminal -- bash
  else
    # -i flag to make it interactive
    # this allows us to use aliases in the command-string passed to k
    gnome-terminal -- bash -ic "$* ; exec bash"
  fi
}

__nemo_kc () {
  if [ $# -eq 0 ]; then
    gnome-terminal -- bash -ic "cd ~; exec bash"
  else
    gnome-terminal -- bash -ic "cd $*; exec bash"
  fi
}

__nemo_kx () {
  if [ $# -eq 0 ]; then
    gnome-terminal -- bash
  else
    gnome-terminal -- bash -ic "$* ; exec bash"
  fi
  exit
}

__nemo_mkcd () {
  mkdir -p $1 && cd $1
}

__nemo_llg () {
  # done specially to avoid grep failing on dir names
  ls -alF | grep -i $* -
}

__nemo_gbk () {
  if [ $# -eq 0 ]; then
    echo "No branch name provided"
  else
    git branch $1 && git checkout $1
  fi
}

# Web shortcuts
# Brave
__nemo_bb () {
  brave-browser "$*" > /dev/null 2>&1 &
}

__nemo_bbn () {
  brave-browser --new-window "$*" > /dev/null 2>&1 &
}

__nemo_bbx () {
  brave-browser "$*" > /dev/null 2>&1 &
  exit
}

__nemo_bbnx () {
  brave-browser --new-window "$*" > /dev/null 2>&1 &
  exit
}

# Other fun little things
__nemo_afk () {
  if [ $# -eq 0 ]; then
    cinnamon-screensaver-command -l
  else
    cinnamon-screensaver-command -l -m "$*"
  fi
}

__nemo_sleepcat () {
  while IFS= read -r i
  do
    echo "$i"
    sleep 0.1
  done
}

# Tmux
__nemo_ta () {
  if [ $# -eq 0 ]; then
    tmux attach
  else
    tmux a -t $1
  fi
}

__nemo_tk () {
  if [ $# -eq 0 ]; then
    tmux kill-session
  else
    tmux kill-session -t $*
  fi
}

# editor
__nemo_np () {
  if [ $# -eq 0 ]; then
    notepad-plus-plus &
  else
    notepad-plus-plus $* &
  fi
}
__nemo_nppx () {
  if [ $# -eq 0 ]; then
    notepad-plus-plus & exit
  else
    notepad-plus-plus $* & exit
  fi
}

__nemo_nq () {
  if [ $# -eq 0 ]; then
    notepadqq &
  else
    notepadqq $* &
  fi
}

__nemo_nqx () {
  if [ $# -eq 0 ]; then
    notepadqq & exit
  else
    notepadqq $* & exit
  fi
}

__nemo_online () {
  echo "Am I online?"
  echo -e "GET http://google.com HTTP/1.0\n\n" | nc google.com 80 > /dev/null 2>&1

  if [ $? -eq 0 ]; then
      echo "Yes"
  else
      echo "No"
  fi
}

repeat() {
  offset=1
  if echo "$1" | grep -qE '^[0-9]+$'; then
    times=$1
    offset+=1
    shift
  else
    times=0
  fi 
  if echo "$1" | grep -qE '^d[0-9]*\.?[0-9]+$'; then
    delay=${1#d}
    shift
  else
    delay=0
  fi
  cmd="$@"
  inc=1
  if [ $times -eq 0 ]; then
    inc=0
    times=1
  fi
  for (( i=0; i<$times; i+=inc)) do
    eval $cmd
    sleep $delay
  done
}

countdown() {
    end="${1:-30}"
    size="${2:-s}"
    mult=1
    msg="${@:2}"
    if [ $size = "m" ]; then
      mult=60
    fi
    if [ $size = "h" ]; then
      mult=3600
    fi
    if [ $mult -ne 1 ]; then
      msg="${@:3}"
    fi
    endSecs=$(echo "$mult * $end" | bc)
    endSecs="${endSecs%\.*}"
    start="$(( $(date '+%s') + $endSecs))"
    while [ $start -ge $(date +%s) ]; do
        time="$(( $start - $(date +%s) ))"
        printf '%s\r' "$(date -u -d "@$time" +%H:%M:%S)"
        sleep 0.1
    done
    notify-send "Countdown complete" "$msg"
    echo
    echo $msg
}


__sw_print_time() {
  start=$1
  ns=$2
  time="$(( $(date +%s) - $start ))"
  timens="$(( 10#$(date +%N) ))"

  nsdiff="$(( timens - ns ))"
  if [[ $nsdiff -le 0 ]]; then
    nsdiff="$(( $nsdiff + 1000000000 ))"
    time="$(( $time - 1 ))"
  fi
  printf '\r%s.%09d' "$(date -u -d "@$time" +%H:%M:%S)" $nsdiff

}


stopwatch() {
    start=$(date +%s)
    startns=10#$(date +%N)
    loop=true
    trap "loop=false" SIGINT
    while $loop; do
        __sw_print_time $start $startns
    done
    echo
    echo
    echo "Total time taken: "
    __sw_print_time $start $startns
    echo
}

__nemo_every () {
  while [ 1 ]; do
    eval "${@:2}"
    sleep $1
    echo
  done
}

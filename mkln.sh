#!/bin/bash
# make a link between ~/.dotfiles/<something> and ~/<something>
# Do nothing silently if the link already exists
# Backup ~/<something> if it is there already

if [ $# -eq 0 ]; then
  echo "No file passed to mkln"
  exit
fi
if [ -f ~/$1 ]; then
  if [ -L ~/$1 ] && [ ~/$1 -ef $(readlink ~/$1) ]; then
    echo "$1 already present"
    exit
  fi
  # back up the currently existing one
  mv ~/$1 ~/$1.bk-$(date +%F--%T)
  echo "Backing up $1"
fi

echo "Linking $1"
ln -s ~/.dotfiles/$1 ~/$1

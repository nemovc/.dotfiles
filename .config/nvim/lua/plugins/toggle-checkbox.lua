-- nvim v0.8.0
return {
  "opdavies/toggle-checkbox.nvim",
  -- setting the keybinding for LazyGit with 'keys' is recommended in
  -- order to load the plugin when the command is run for the first time
  keys = {
    { "<leader>tc", ":lua require('toggle-checkbox').toggle()<CR>", desc = "Toggle Checkbox (markdown)"}
  },
}

/**
 * Macros that allow us to open a search term in our most commonly used sites
 *
 *
 */

module.exports = {
  Google: {
    key: "go",
    address: "https://www.google.com/search?q=$*",
  },
  "Stack Overflow": {
    key: "so",
    address: "https://www.stackoverflow.com/search?q=$*",
  },
  YouTube: {
    key: "yt",
    address: "https://www.youtube.com/results?search_query=$*",
  },
  NPM: {
    key: "ns",
    address: "https://www.npmjs.com/package/$*",
  },
  "CanIUse?": {
    key: "caniuse",
    address: "https://caniuse.com/#search=$*",
  },
  eslint: {
    key: "esl",
    address: "https://eslint.org/docs/latest/rules/$*",
  },
  Wikipedia: {
    key: "wk",
    address: "https://en.wikipedia.org/wiki/$*",
  },
  Facebook: {
    key: "fb",
    address: "https://www.facebook.com",
  },
  GMail: {
    key: "gmail",
    address: "https://mail.google.com",
  },
  "Google Sheets": {
    key: "sheets",
    address: "https://docs.google.com/spreadsheets/u/0/",
  },
  "Google Docs": {
    key: "docs",
    address: "https://docs.google.com/document/u/0/",
  },
  "Google Drive": {
    key: "gdrive",
    address: "https://drive.google.com/drive/u/0/my-drive",
  },
  XKCD: {
    key: "xkcd",
    address: "https://xkcd.com/",
  },
  SMBC: {
    key: "smbc",
    address: "https://www.smbc-comics.com/",
  },
  "Google maps": {
    key: "maps",
    address: "https://google.com/maps/search/$*",
  },
};

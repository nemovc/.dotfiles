@echo off
choice /m "Setup nemo dev environment?"
if %ERRORLEVEL% neq 1 (
  echo Then why'd you run the command, idiot? Stop sipping dumb-fuck juice.
  exit /B
)
echo Prepare for trouble, and make it double!
echo And by that I mean don't worry about all the shit that's about to happen
timeout 2
echo Desktop 1: Shared

rem first window: set up opera with useful links, slack, notes, spotify
vdesk on:1 run:%opl% --new-window https://trello.com/b/at3lRQDt/supernova https://docs.google.com/spreadsheets/d/1rzNCxMuq_76PVti0OhaagB6rVHgFdtclMWkFeE37_gg https://mail.google.com/mail/u/0 https://console.firebase.google.com/u/0/project/supernova-licensing-dev/overview https://console.firebase.google.com/u/0/project/supernova-licensing/overview
vdesk on:1 run:%appl%\slack\slack.exe
vdesk on:1 run:%appd%\spotify\spotify.exe

rem the way we do notes is fucking retarded, thanks microsoft
vdesk on:0 run:explorer.exe shell:appsFolder\Microsoft.MicrosoftStickyNotes_8wekyb3d8bbwe!App

timeout 6

echo Desktop 2: Lodestar
vdesk on:2 run:cmd.exe /k "cd %el%"
vdesk on:2 run:cmd.exe /k "cd %el% && git status"
vdesk on:2 run:atom %el%
vdesk on:2 run:explorer %el%
vdesk on:2 run:%opl% --new-window https://bitbucket.org/eantedevelopment/tieup/commits/

timeout 6

echo Desktop 3: Supernova-licensing-backend
vdesk on:3 run:cmd.exe /k "cd %eb%"
vdesk on:3 run:cmd.exe /k "cd %eb% && git status"
vdesk on:3 run:atom %eb%
vdesk on:3 run:explorer %eb%
vdesk on:3 run:%opl% --new-window https://bitbucket.org/eantedevelopment/supernova-licensing-backend/commits/

timeout 6

echo Desktop 4: Supernova-licensing-frontend
vdesk on:4 run:cmd.exe /k "cd %ef%"
vdesk on:4 run:cmd.exe /k "cd %ef% && git status"
vdesk on:4 run:atom %ef%
vdesk on:4 run:explorer %ef%
vdesk on:4 run:%opl% --new-window https://bitbucket.org/eantedevelopment/supernova-licensing-frontend/commits/

timeout 6

echo Desktop 4: Supernova-mine-panel
vdesk on:5 run:cmd.exe /k "cd %em%"
vdesk on:5 run:cmd.exe /k "cd %em% && git status"
vdesk on:5 run:atom %em%
vdesk on:5 run:explorer %em%
vdesk on:5 run:%opl% --new-window https://bitbucket.org/eantedevelopment/supernova-mine-panel/commits/

echo Done
vdesk 0

// shared functions for management scripts

const {
  exec,
} = require('child_process');

const { readFileSync, existsSync } = require('fs');
const { sep } = require('path');

const terminalColors = {
  reset: '\x1b[0m',
  none: '',
  // Foreground
  red: '\x1b[31m',
  green: '\x1b[32m',
  yellow: '\x1b[33m',
};

function log(msg, color = 'none', bright = false) {
  console.log(`${terminalColors[color]}${bright ? '\x1b[1m' : ''}${msg}${terminalColors.reset}`);
}

async function asyncForEach(arr, cb) {
  for (let i = 0; i < arr.length; i++) {
    // eslint-disable-next-line no-await-in-loop
    await cb(arr[i], i);
  }
}

function execWrap(cmd, {
  output = true, showLog = true, stdoutColor = 'none', stderrColor = 'red',
} = {}) {
  return new Promise((resolve) => {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        if (showLog) {
          log(stderr.slice(0, -1), stderrColor);
        }
        resolve(output ? stdout : false);
      } else if (stdout) {
        if (showLog) {
          log(stdout.slice(0, -1), stdoutColor);
        }
        resolve(output ? stdout : true);
      } else {
        resolve(true);
      }
    });
  });
}


async function mkln(files) {
  const arr = Array.isArray(files) ? files : [files];
  await asyncForEach(arr, async (file) => {
    await execWrap(`bash mkln.sh ${file}`);
  });
}

async function mklndir(dirs) {
  const arr = Array.isArray(dirs) ? dirs : [dirs];
  await asyncForEach(arr, async (dir) => {
    await execWrap(`bash mklndir.sh ${dir}`);
  });
}

async function aptInst(packages) {
  const pstr = Array.isArray(packages) ? packages.join(' ') : packages;
  console.log('Apt packages: ', terminalColors.yellow, pstr, terminalColors.reset);
  await execWrap(`apt install ${pstr} -y`);
}

async function addAptRepo(repo) {
  const rstr = Array.isArray(repo) ? repo.join(' ') : repo;
  console.log('Apt repos: ', terminalColors.yellow, rstr, terminalColors.reset);
  await execWrap(`add-apt-repository ${rstr}`);
}

async function aptUpdate() {
  await execWrap('apt-get update', { showLog: false });
  log('Done');
}

async function npmInst(packages) {
  const pstr = Array.isArray(packages) ? packages.join(' ') : packages;
  console.log('NPM packages: ', terminalColors.yellow, pstr, terminalColors.reset);
  await execWrap(`npm -g install ${pstr}`);
}

async function pipInst(packages) {
  const pstr = Array.isArray(packages) ? packages.join(' ') : packages;
  console.log('PIP packages: ', terminalColors.yellow, pstr, terminalColors.reset);
  await execWrap(`pip install ${pstr}`);
}

async function snapInst(packages) {
  // have to install one by 1 if they contain --classic
  if (Array.isArray(packages)) {
    const grouped = [];
    const single = [];
    packages.forEach((pack) => {
      if (pack.indexOf('--') === -1) {
        grouped.push(pack);
      } else {
        single.push(pack);
      }
    });
    await asyncForEach([...single, grouped.join(' ')], async (pstr) => {
      console.log('Snap packages: ', terminalColors.yellow, pstr, terminalColors.reset);
      await execWrap(`snap install ${pstr}`);
    });
  } else {
    console.log('Snap packages: ', terminalColors.yellow, packages, terminalColors.reset);
    await execWrap(`snap install ${packages}`);
  }
}

function fileContentsIdentical(paths) {
  const buf = readFileSync(paths[0]);
  for (let i = 1; i < paths.length; i++) {
    const buf2 = readFileSync(paths[i]);
    if (!buf.equals(buf2)) {
      return false;
    }
  }
  return true;
}

function readConfFile(path) {
  const out = [];
  readFileSync(path).toString().split('\n').forEach((line) => {
    if (line[0] !== '#' && line.length !== 0) {
      out.push(line);
    }
  });
  return out;
}

async function download(url, file) {
  const filePath = `~/.dotfiles/download/${file}`;
  if (!existsSync(filePath)) {
    console.log(`Downloading ${file}`);
    await execWrap(`curl -L --output ${filePath} ${url}`);
  }
  return filePath;
}

async function checkCommand(command) {
  const exists = await execWrap(`which ${command}`);
  return exists !== '\n';
}


async function gitClone({
  repo, dirName = '', dir = '~/code', branch = 'master', afterInstall = [],
} = {}) {
  if (typeof repo === 'undefined') {
    log('No repo to load', 'red');
    return false;
  }
  let fileName;
  if (dirName === '') {
    // get dirName from the '.git' portion of the repo
    [, fileName] = /.+\/(.+)\.git/.exec(repo);
  } else {
    fileName = dirName;
  }
  const fullPath = `${dir}/${fileName}`;
  log(`Cloning ${fileName}`, 'green');
  if (existsSync(fullPath)) {
    log('Already there', 'green');
    return true;
  }
  await execWrap(`mkdir -p ${dir}`);
  await execWrap(`git clone ${repo} ${fullPath}`);
  await execWrap(`cd ${fullPath}; git checkout ${branch}`);
  if (afterInstall.indexOf('submodules') !== -1) {
    await execWrap(`cd ${fullPath};git submodule init; git submodule update;`);
  }
  if (afterInstall.indexOf('npm') !== -1) {
    console.log('Running NPM install');
    await execWrap(`cd ${fullPath}; npm install`);
  }
  // fix incorrect owner and group
  await execWrap(`cd ${fullPath};chown $(logname) -R .; chgrp $(logname) -R .;`);
  return true;
}

/**
 * checkSubpackges
 * 
 * Allows us to check a list of packages against a file
 * 
 * @param {String} name - which type of package this is
 * @param {String} file - Path of list file, relative to ~/.dotfiles e.g., '.vscodeextensions'
 * @param {String} getCommand - Command to generate list of currently installed packages (one per line) e.g., 'code --list-extensions'
 * @param {String} installCommand - Command to install package e.g., 'code --install-extension '
 * @param {String} removeCommand - Command to remove a package e.g., 'code --uninstall-extension 
 * @param {Boolean = false} installation - Whether or not to treat this as a brand new installation (i.e., don't read current package list)
 */
async function checkSubpackages({
  name,
  file,
  getCommand,
  installCommand,
  removeCommand,
  installation=false,
}){
  log(`Checking ${name} packages`)
  const shouldHave = await readConfFile(file);
  const current = (await execWrap(getCommand, {showLog: false})).split('\n').slice(0,-1);
  const add = [];
  const remove = [];
  shouldHave.forEach((pack) => {
    if(current.indexOf(pack) === -1){
      add.push(pack);
    }
  });
  if(!installation){
    current.forEach((pack)=>{
      if(shouldHave.indexOf(pack) === -1){
        remove.push(pack);
      }
    })
  }
  log(`Adding ${add.length} ${name} packages`, 'yellow');
  await asyncForEach(add, async (pack) => {
    log(`${pack}`, 'green');
    await execWrap(`${installCommand} ${pack}`);
  })
  log(`Removing ${remove.length} ${name} packages`, 'yellow');
  await asyncForEach(remove, async (pack) => {
    log(`${pack}`, 'red');
    await execWrap(`${removeCommand} ${pack}`);
  })
}

module.exports = {
  execWrap,
  mkln,
  mklndir,
  aptInst,
  addAptRepo,
  aptUpdate,
  npmInst,
  pipInst,
  fileContentsIdentical,
  log,
  readConfFile,
  snapInst,
  download,
  checkCommand,
  gitClone,
  checkSubpackages,
};

#o ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
*i*) ;;
*) return ;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm* | rxvt*)
  PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
  ;;
*) ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  # alias ls='ls --color=auto'
  #alias dir='dir --color=auto'
  #alias vdir='vdir --color=auto'

  # alias grep='grep --color=auto'
  # alias fgrep='fgrep --color=auto'
  # alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
# now in macros (so we can source them separately)
# alias ll='ls -alF'
# alias la='ls -A'
# alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
# alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Include git-complete and git-prompt scripts
if [ -f ~/.dotfiles/.git-completion.bash ]; then
  . ~/.dotfiles/.git-completion.bash

  # Add our aliases for git-completion
  __git_complete g __git_main
  __git_complete gp _git_pull
  __git_complete gk _git_checkout
  __git_complete gm _git_merge
  __git_complete gd _git_diff
  __git_complete gs _git_status
  __git_complete gb _git_branch
fi

if [ -f ~/.dotfiles/.bash_prompt ]; then
  . ~/.dotfiles/.bash_prompt
fi

# Function alias definitions
if [ -f ~/.dotfiles/.bash_functions ]; then
  . ~/.dotfiles/.bash_functions
fi

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -f ~/.dotfiles/.bash_aliases ]; then
  . ~/.dotfiles/.bash_aliases
fi

# Add our custom dirfuncs
if [ -f ~/.dotfiles/.bash_alias_functions ]; then
  . ~/.dotfiles/.bash_alias_functions
fi

# Add our custom webfuncs
if [ -f ~/.dotfiles/.bash_web_functions ]; then
  . ~/.dotfiles/.bash_web_functions
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Set up ENV vars
export ATOM_HOME=~/.dotfiles/.atom
# export GOOGLE_APPLICATION_CREDENTIALS="/home/owen/Downloads/lodestar-5ec6b-1ba17d270879-supernova.json"
# export GOOGLE_APPLICATION_CREDENTIALS="/home/owen/Downloads/supernova-licensing-dev-4ffb97030cc2.json"
# export GOOGLE_APPLICATION_CREDENTIALS="/home/owen/Downloads/supernovamining-c4799cb86104-eante.json"
export NOTES_CONFIG_PATH=~/.config/notes/config.json
export PATH="$PATH:/snap/bin:/usr/bin/python:~/.pyenv/bin:~/.poetry/bin:~/.yarn/bin:/opt/nvim-linux-x86_64/bin:~/.local/bin"

export ANDROID_HOME=/home/owen/Android/Sdk
export ANDROID_SDK_ROOT=/home/owen/Android/Sdk

export JAVA_HOME="/usr/lib/jvm/java-17-openjdk-amd64"

#eval "$(pyenv init -)"
#eval "$(pyenv virtualenv-init -)"

# attach to tmux, but not in guake

# create special tmux session for guake
if [ -n "$GUAKE_TAB_UUID" ] && [ -z "$TMUX" ]; then
  tmux new-session -A -s guake
elif [ -z "$TMUX" ]; then
  if [ -n "$SSH_CONNECTION" ]; then
    # create and connect to a tmux server with the appropriate env variables
    tmux new-session -A -s ssh
  else
    tmux new-sessio -A -s main
  fi
fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"                    # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion" # This loads nvm bash_completion

# add npm complection
source ~/.dotfiles/.npm_completion

# Use bash completion (aliases)
source ~/.dotfiles/.bash_completion

# Enable autocdo
shopt -s autocd
# eval "$(thefuck --alias)"

# add ANDROID_HOME and android SDK
export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/platform-tools

# source ~/.config/broot/launcher/bash/br
# eval "$(thefuck --alias --enable-experimental-instant-mode)"

. "$HOME/.cargo/env"
export EDITOR=nvim
export VISUAL=nvim

source ~/.dotfiles/.motd

source ~/.config/broot/launcher/bash/br
# add broot completion
source ~/.dotfiles/.broot_completion.bash

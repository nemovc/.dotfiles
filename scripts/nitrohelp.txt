Usage:
	--force-setter=[arg]
		Force setter engine: xwindows, xinerama, gnome, pcmanfm
	--head=[arg]
		Select xinerama/multihead display in GUI, 0..n, -1 for full
	--help, -h
		Prints this help text.
	--no-recurse
		Do not recurse into subdirectories
	--random
		Choose random background from config or given directory
	--restore
		Restore saved backgrounds
	--save
		Saves the background permanently
	--set-auto
		Sets the background to the given file (auto)
	--set-centered
		Sets the background to the given file (centered)
	--set-color=[arg]
		background color in hex, #000000 by default
	--set-scaled
		Sets the background to the given file (scaled)
	--set-tiled
		Sets the background to the given file (tiled)
	--set-zoom
		Sets the background to the given file (zoom)
	--set-zoom-fill
		Sets the background to the given file (zoom-fill)
	--sort=[arg]
		How to sort the backgrounds. Valid options are:
			* alpha, for alphanumeric sort
			* ralpha, for reverse alphanumeric sort
			* time, for last modified time sort (oldest first)
			* rtime, for reverse last modified time sort (newest first)

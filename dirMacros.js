/*
  Macros that allow us to navigate to common dirs

  1) The name of the macro will cd to that dir i.e., 'dotfiles'
  2) 'ud <name>' will pushd to that location
  3) '[e|n] <name> will open a file browser in that location
  4) 'c <name>' will open vscode in that location
  5) 'a <name>' will open atom in that location
  
  If the name is a folder in the current location, that will be used for priority
*/

// Set base paths

const selfPath = {
  linux: '~/code/self/',
}[process.platform];

const adacsPath = {
  linux: '~/code/adacs/',
}[process.platform];

module.exports = {
  commands: {
    c: { command: 'code', amp: '&' },
    n: { command: 'nautilus', amp: '&' },
    nv: { command: 'pushd > /dev/null', amp: '&& nvim . && popd > /dev/null' },
    nvf: { command: 'nvim', amp: '' },
    prnv: { command: "poetry run nvim", amp: '' },
    e: { command: 'nemo', amp: '&' },
    ud: { command: 'pushd', amp: '' },
    cdl: { command: 'cd', amp: '&& ls -la' },
    br: { command: 'broot', amp: '' },
    ll: { command: 'ls -lah', amp: '' },
  },
  paths: {

    '..': '..',
    '...': '../..',
    '....': '../../..',
    '.....': '../../../..',
    galaxy: `${selfPath}galaxy-survival`,
    adacs: `${adacsPath}`,
    gwcloud: `${adacsPath}/gwcloud`,
    jsd: '~/code/jsdoodles',
    coded: '~/code',
    self: selfPath,
    dotfiles: '~/.dotfiles',
    uprof: '~',
    '~': '~',
    config: '~/.config',
    doc: '~/Documents',
    desk: '~/Desktop',
    down: '~/Downloads',
    tmp: '/tmp',
    mcd: '~/.minecraft'
  }
}

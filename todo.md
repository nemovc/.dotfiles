
# get a laptop dock

- or maybe try and see what the instal process is like on WSL on my main pc

# back up google authenticator

# install and configure zsh and ohmyz.sh

- need to ensure that this is done in such a way that it is replicable
- copy relevant bits of .bashrc to .zshrc or equivalent
- clean up PS1

- also configure nvim
  - show hidden files in tree by default
  - check vscode config, snippets for things i still want

# make tmux look better (menu bar could use some love)

# configure python

- pyenv, pyenv-virtualenv
- poetry
- install system-wide python packages
- virtualenv through pipx

# configure ubuntu desktop

- keyboard shortcuts
- window appearence
- workspaces
- desklets (IM GOING TO ACTUALLY USE CONKY)
  - conky config at [https://github.com/madhur/awesome-conky] is a place to start
- ensure that we can save and back up this
- fingerprint login
- brave-browser vimbinds
- find a tmux keybind for last-window

# update install script to include gui packages

- slack-term
- discord-term
-
- what else?

# refactor package lists to include comments, sectors

# update install script to recommend signing into services

- outlook
- facebook
- google
- slack, slack-term
- discord, discord-term
- spotify, spotify-term
- steam
- brave sync
- bitwarden (both in browser and in app and in CLI)
  - and also configure settings if required, making timeout harsher etc

- [ ] what else?
- [ ] would be cool if i could make this as a conky TODO

# update install script to only run as user in correct dir

# include .ssh/config in installscript/dotfiles

# add reminder to update this config

- [ ] run weekly
- [ ] actually write update/sync script

# LEARN

learn how to frequently use the following packages/programs

- nvim (DUH)
- btop
- duf
- lazygit
- oha
- gping
- broot

# flatpak

Add flatpak installation and the following packages

- h5 viewer org.hdfgroup.HDFView

# tpm
- [ ] figure out a way to have tpm automatically install plugins

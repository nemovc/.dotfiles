# Your init script
#
# Atom will evaluate this file each time a new window is opened. It is run
# after packages are loaded/activated and after the previous editor state
# has been restored.
#
# An example hack to log to the console when each text editor is saved.
#
# atom.workspace.observeTextEditors (editor) ->
#   editor.onDidSave ->
#     console.log "Saved! #{editor.getPath()}"

atom.commands.add 'atom-workspace', 'nemo:semicolonize-newline', ->
  editor = atom.workspace.getActiveTextEditor()
  editor.moveToEndOfLine()
  editor.insertText(";")
  atom.commands.dispatch(atom.views.getView(editor), 'editor:newline')

atom.commands.add 'atom-workspace', 'nemo:semicolonize', ->
  editor = atom.workspace.getActiveTextEditor()
  point = editor.getCursorBufferPosition()
  editor.moveToEndOfLine()
  editor.insertText(";")
  editor.setCursorBufferPosition(point)

atom.commands.add 'atom-workspace', 'nemo:commaize-newline', ->
  editor = atom.workspace.getActiveTextEditor()
  editor.moveToEndOfLine()
  editor.insertText(",")
  atom.commands.dispatch(atom.views.getView(editor), 'editor:newline')

atom.commands.add 'atom-workspace', 'nemo:commaize', ->
  editor = atom.workspace.getActiveTextEditor()
  point = editor.getCursorBufferPosition()
  editor.moveToEndOfLine()
  editor.insertText(",")
  editor.setCursorBufferPosition(point)

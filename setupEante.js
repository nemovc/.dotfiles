const {
  execWrap,
  mkln,
  aptInst,
  addAptRepo,
  aptUpdate,
  npmInst,
  snapInst,
  fileContentsIdentical,
  log,
  readConfFile,
  checkCommand,
  download,
  gitClone,
} = require('./manage.js');


async function setupEante() {
  log('\nSetting up Eante repos');
  console.log('Applying git hack');
  execWrap('git credential-cache exit');
  // Lodestar
  await gitClone({
    repo: 'https://bitbucket.org/eantedevelopment/lodestar.git',
    dirName: 'lodestar',
    dir: '../code/eante',
    branch: 'develop',
    afterInstall: ['submodules', 'npm'],
  });
  await gitClone({
    repo: 'https://bitbucket.org/eantedevelopment/supernova-licensing-frontend.git',
    dir: '../code/eante',
    branch: 'develop',
    afterInstall: ['submodules', 'npm'],
  });
  await gitClone({
    repo: 'https://bitbucket.org/eantedevelopment/supernova-licensing-backend.git',
    dir: '../code/eante',
    branch: 'develop',
    afterInstall: ['submodules', 'npm'],
  });
  await gitClone({
    repo: 'https://bitbucket.org/eantedevelopment/supernova-mine-panel.git',
    dir: '../code/eante',
    branch: 'develop',
    afterInstall: ['submodules', 'npm'],
  });

  console.log('Resetting git hack');
  execWrap('git credential-cache exit');
}

setupEante();

// full install that downloads and installs all the relevant programs

const { existsSync } = require("fs");
const quickInstall = require("./quickInstall");
const {
  execWrap,
  mkln,
  mklndir,
  aptInst,
  addAptRepo,
  aptUpdate,
  npmInst,
  snapInst,
  pipInst,
  fileContentsIdentical,
  log,
  readConfFile,
  checkCommand,
  download,
  checkSubpackages,
} = require("./manage.js");

async function fullInstall() {
  console.log("Full install");

  await quickInstall(false);

  console.log("\nAdding apt repos");
  await addAptRepo(readConfFile(".aptrepos"));

  console.log("\nApt update");
  await aptUpdate();

  console.log("\nAdding apt packages");
  await aptInst(readConfFile(".aptpackages"));

  console.log("\nAdding snap packages (this can take a while)");
  console.log("Actually, not doing that lol, snap is now bad for mint");
  // await snapInst(readConfFile('.snappackages'));

  console.log("\nAdding NPM packages (this can also take a while)");
  await npmInst(readConfFile(".npmglobalpackages"));

  console.log("\nAdding pip packages (this can also take a while)");
  await pipInst(readConfFile(".pippackages"));

  console.log("\nInstalling firacode");
  await execWrap("mkdir -p ~/.local/share/fonts");
  await execWrap("cp firacode/* ~/.local/share/fonts/");
  console.log("Rebuilding font cache");
  await execWrap("fc-cache -f -v", { showLog: false });
  console.log("Done");

  console.log("\nCreating ~/code and ~/code/self");
  await execWrap("mkdir -p ~/code/self");
  await execWrap("chown -R $(logname):$(id -g -n $(logname)) ~/code");

  console.log("Installing custom things");
  console.log("slack-term");
  if (await checkCommand("slack-term")) {
    const slackTerm = await download(
      "https://github.com/erroneousboat/slack-term/releases/download/v0.4.1/slack-term-linux-amd64",
      "slack-term"
    );
    console.log("Downloaded");
    await execWrap(`chmod +x ${slackTerm}`);
    await execWrap(`mv ${slackTerm} /usr/local/bin`);
    if (!existsSync("../.slack-term")) {
      log(
        "Set up slack-term by creating a ~/.slack-term file containing your slack token",
        "yellow"
      );
      console.log("https://github.com/erroneousboat/slack-term#setup");
    }
    console.log("Done slack-term");
  } else {
    console.log("Already present");
  }

  console.log("Installing vscode");
  if (await checkCommand("code")) {
    await mkln([".config/Code"]);
    await execWrap(
      "curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg"
    );
    await execWrap(
      "install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/"
    );
    await execWrap(
      `sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'`
    );
    await execWrap("apt-get install apt-transport-https");
    await execWrap("apt-get update");
    await execWrap("apt-get install code");
    await checkSubpackages({
      name: "vscode",
      file: ".vscodeextensions",
      getCommand: "code --list-extensions",
      installCommand:
        "code --user-data-dir ~/.config/Code --install-extension ",
      removeCommand:
        "code --user-data-dir ~/.config/Code --uninstall-extension ",
      installation: true,
    });
  } else {
    console.log("Already present");
  }

  console.log("Installing opera");
  if (await checkCommand("opera")) {
    // https://linux4one.com/how-to-install-opera-browser-on-linux-mint-19/
    await execWrap(
      "wget -qO- https://deb.opera.com/archive.key | sudo apt-key add -"
    );
    await execWrap(
      "echo deb https://deb.opera.com/opera-stable/ stable non-free | sudo tee /etc/apt/sources.list.d/opera.list"
    );
    await execWrap("apt-get update");
    await execWrap("apt-get install opera-stable -y");
  } else {
    console.log("Already present");
  }

  console.log("Installing spotify");
  if (await checkCommand("spotify")) {
    await execWrap(
      "curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add -"
    );
    await execWrap(
      'echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list'
    );
    await execWrap("apt-get update");
    await execWrap("apt-get install spotify-client");
  } else {
    console.log("Already present");
  }

  console.log("Installing discord");
  if (await checkCommand("discord")) {
    await download(
      "https://discordapp.com/api/download?platform=linux&format=deb",
      "discord.deb"
    );
    await execWrap("dpkg -i ~/.dotfiles/download/discord.deb");
  } else {
    console.log("Already present");
  }

  console.log("Installing slack");
  if (await checkCommand("slack")) {
    await download(
      "https://downloads.slack-edge.com/linux_releases/slack-desktop-4.12.2-amd64.deb",
      "slack.deb"
    );
    await execWrap("dpkg -i ~/.dotfiles/download/slack.deb");
  } else {
    console.log("Already present");
  }

  log("Installing pyenv & virtualenv");
  if (await checkCommand("pyenv")) {
    await execWrap("git clone https://github.com/pyenv/pyenv.git $HOME/.pyenv");
    await execWrap(
      "git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv"
    );
  } else {
    log("Already present)");
  }

  log("Installing poetry");
  if (await checkCommand("poetry")) {
    await execWrap(
      "curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python"
    );
  } else {
    log("Already present");
  }

  log("Link cinnamon spices and spice configs");
  log("Skipped, because new version, new me", "red");
  // await mklndir([".cinnamon/configs", ".local/share/cinnamon"]);

  // add startup processes
  log("Adding autostart scripts");
  await execWrap("rmdir ~/.config/autostart");
  await mkln(".config/autostart");

  // doens't work due to permissions
  // dconf has to run as the local user, but for some reason the sudo isn't working as expected
  // log('Loading dconf');
  // await execWrap('sudo -u $(logname) dconf load / < .dconfDump', {showLog: true})

  // fix some permissions issues
  log("\nFixing permissions");
  await execWrap("chown -R $(logname):$(id -g -n $(logname)) .");
  await execWrap("chown -R $(logname):$(id -g -n $(logname)) ~/.config");
  await execWrap("chown -R $(logname):$(id -g -n $(logname)) ~/.pyenv");
  await execWrap("chown -R $(logname):$(id -g -n $(logname)) ~/.poetry");

  log("Things still to do", "green", true);
  // see above
  log("dconf load / < .dconfDump", "red", true);
  log("Load tmux plugins (` + I)");
  log("Verify graphics driver");
  log("Add ssh keys");
  log("Log into BitWarden");
  log("Log into opera");
  log("Log into google/gmail");
  log("log into discord");
  log("log into slack");
  log("log into facebook");
  log("log into spotify");
  log("install nvm ");
  log(
    "go through installed-packages (and the commented section of .aptpackages) finding list of packages I actually want"
  );
  log("create a curated list of these and update this installer");
  log("Configure desktop environment:", "green");
  log("don't forget to somehow list/save the settings changed", "yellow");
  log("background");
  log("keybinds");
  log("> windowing system");
  log("> ???");
  log("panels");
  log("applets");
  log("desklets");
  log("system settings");
  log("set up work enviroments");
  log("set up local ~/code dir");
  log("set up steam");
}

fullInstall();

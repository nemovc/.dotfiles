// Update the remote .dotfiles
// This will create a list of any changes to atom packages, pull in any changes
// to the files we manage outside ~/.dotfiles, and then, if required, commit and push the changes


// notes
//
// .gitconfig changes
// check for any changes to .gitconfig (i.e., diff to HEAD)
// if any, stop and ask the the user to make the changes to either .gitinclude-<os> or manually
// commit the changes to .gitconfig (if operating-system agnostic)
//
// then, check if .gitinclude and .gitinclude-<os> differ - if so, ask the user to fix it manually
// push changes to .dconfDump
// changes to .atompackages and .codepackages

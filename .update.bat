@echo off
cd %dotfiles%
echo.
echo nemovc dotfiles and environment update
echo.
echo Saving atom packages
call apm list -bi > .atompackages
echo.
echo Cloning .gitconfig
git diff-index --quiet HEAD .gitconfig
if %errorlevel% neq 0 (
  echo .gitconfig has been modified directly - please merge changes you've made to %dotfiles%\.gitconfig into %uprof%\.gitconfig manually, and reset changes made to %dotfiles%\.gitconfig back to HEAD
  exit /b
)
if exist .gitconfig (del .gitconfig)
copy %userprofile%\.gitconfig %userprofile%\.dotfiles\.gitconfig
echo Done
echo.
echo Checking if anything has changed
git diff-index --quiet HEAD
if %ERRORLEVEL% neq 0 (
  setlocal EnableDelayedExpansion
  echo Files updated
  echo.

  echo Dealing with git
  git add .
  git commit -m "Automatic update"
  git push
) else (
  echo Nothing changed.
)


REM cd !origdir!
echo Update completed
